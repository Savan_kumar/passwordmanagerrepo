/*
 * *****************************************************************************
 * AddSiteViewModelClass.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 2/10/20 12:18 PM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.viewmodel

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.Observable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.robosoft.passwordmanagerapp.R
import com.robosoft.passwordmanagerapp.helper.CipherText
import com.robosoft.passwordmanagerapp.helper.encrypt
import com.robosoft.passwordmanagerapp.model.SiteDetailClass
import com.robosoft.passwordmanagerapp.model.database.UserDataRepository
import com.robosoft.passwordmanagerapp.view.PassHomeActivity
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class AddSiteViewModelClass( val dataRepository: UserDataRepository) : ViewModel(), Observable {

    lateinit var context: Context
    val siteUrlValue = MutableLiveData<String>()
    val siteNameValue = MutableLiveData<String>()
    val siteTypeValue = MutableLiveData<String>()
    val siteUserNameValue = MutableLiveData<String>()
    val siteUserPasswordValue = MutableLiveData<String>()
    val siteNotesValue = MutableLiveData<String>()
    val sitePasswordByteArray = MutableLiveData<ByteArray>()
    val siteIVKey = MutableLiveData<ByteArray>()
    val siteCipherKey = MutableLiveData<String>()
    var userMobileNumber = MutableLiveData<String>()
    val progressRate = MutableLiveData<Int>()
    val isUrlEmpty = MutableLiveData<Boolean>()
    val isSiteNameEmpty = MutableLiveData<Boolean>()
    val isSiteTypeEmpty = MutableLiveData<Boolean>()
    val isSiteUserNameEmpty = MutableLiveData<Boolean>()
    val isSiteUserPasswordEmpty = MutableLiveData<Boolean>()
    var dataEntry : Long = 0
    val onInitialExecution = MutableLiveData<Boolean>()
    val afterAdd = MutableLiveData<Boolean>()
    init{
        onInitialExecution.value = true
        siteNotesValue.value = ""
    }
    fun onSaveBtnClicked(view: View) {
        context = view.context
        var result = checkDataFields()
        if(result) {
            var cipherValues = siteUserPasswordValue.value!!.encrypt()
            addSiteData(cipherValues)


        }
    }
    private fun checkDataFields(): Boolean {
        when {
            siteUrlValue.value == null -> {
                isUrlEmpty.value = true
                return false
            }
            siteNameValue.value == null -> {
                isSiteNameEmpty.value = true
                return false
            }
            siteTypeValue.value == null -> {
                isSiteTypeEmpty.value = true
                return false
            }
            siteUserNameValue.value == null -> {
                isSiteUserNameEmpty.value = true
                return false
            }
            siteUserPasswordValue.value == null -> {
                isSiteUserPasswordEmpty.value = true
            }
            else -> {
                return true
            }
        }
        return true
    }
    fun onResetBtnClicked(view : View) {
        siteUrlValue.value = null
        siteNameValue.value = null
        siteTypeValue.value = null
        siteUserNameValue.value = null
        siteUserPasswordValue.value = null
        siteNotesValue.value = null

    }
    fun addSiteData(cipherText: CipherText) {
        sitePasswordByteArray.value = cipherText.cipherText
        siteIVKey.value = cipherText.iv
        siteCipherKey.value = cipherText.key
        insertSiteData(SiteDetailClass(0, userMobileNumber.value!!, siteUrlValue.value!!, siteNameValue.value!!, siteTypeValue.value!!, siteUserNameValue.value!!,sitePasswordByteArray.value!!, siteNotesValue.value!!, siteIVKey.value!!, siteCipherKey.value!! ))
    }
    private fun insertSiteData(siteDetailClass: SiteDetailClass): Job = viewModelScope.launch {
        dataEntry = dataRepository.insertSiteData(siteDetailClass)
        if(dataEntry == null) {
            errorInsertingData()
        } else {
            Log.d("MyTagDataEntry", dataEntry.toString())
            addSiteSuccess()
            val intent = Intent(context, PassHomeActivity::class.java)
            context.startActivity(intent)
            afterAdd.value = true

        }
    }
    private fun errorInsertingData() {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val customLayoutToast: View = inflater.inflate(R.layout.custom_toast_message, null)
        (customLayoutToast.findViewById<View>(R.id.tvToastMessageOne) as TextView).text = context.getString(
                    R.string.error_inserting_site_data)

        //show toast
        val customToast = Toast(context)
        customToast.view = customLayoutToast
        customToast.duration = Toast.LENGTH_SHORT
        customToast.show()
    }
    private fun addSiteSuccess() {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val customLayoutToast: View = inflater.inflate(R.layout.custom_toast_message, null)
        (customLayoutToast.findViewById<View>(R.id.tvToastMessageOne) as TextView).text = context.getString(
                    R.string.added_site_success)

        //show toast
        val customToast = Toast(context)
        customToast.view = customLayoutToast
        customToast.duration = Toast.LENGTH_SHORT
        customToast.show()
    }
    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        //
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        //
    }
}