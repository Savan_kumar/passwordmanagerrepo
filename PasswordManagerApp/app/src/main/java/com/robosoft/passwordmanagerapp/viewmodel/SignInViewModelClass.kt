/*
 * *****************************************************************************
 * SignInViewModel.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 30/9/20 2:56 AM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.viewmodel

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.robosoft.passwordmanagerapp.R
import com.robosoft.passwordmanagerapp.helper.decrypt
import com.robosoft.passwordmanagerapp.model.UserClass
import com.robosoft.passwordmanagerapp.model.database.UserDataRepository

import com.robosoft.passwordmanagerapp.view.PassHomeActivity


class SignInViewModelClass( val dataRepository: UserDataRepository) : ViewModel(), Observable {

    lateinit var userList: List<UserClass>
    lateinit var context: Context

    @Bindable
    val userMobileNumber = MutableLiveData<String>()
    @Bindable
    val userMPin = MutableLiveData<String>()
    @Bindable
    val isValidMobileNumber = MutableLiveData<Boolean>()
    @Bindable
    val isValidMPin = MutableLiveData<Boolean>()
    @Bindable
    val isMobileNumberEmpty = MutableLiveData<Boolean>()
    @Bindable
    val isMPinEmpty = MutableLiveData<Boolean>()

    fun onSignInClicked(view: View) {
        context = view.context
        if(userMobileNumber.value == null){
         isMobileNumberEmpty.value = true
        } else if(userMobileNumber.value!!.length!=10) {
            isValidMobileNumber.value = true
        }
        else if(userMPin.value == null) {
            isMPinEmpty.value = true
        } else if(userMPin.value!!.length!=4) {
            isValidMPin.value = true
        }
        else {
            //get user detail from db and validate user
            //view.context.startActivity(Intent(view.context, PassHomeActivity::class.java))
            userList = dataRepository.getUserByPhone(userMobileNumber.value!!)
            var enteredPassword = userMPin.value
            if(userList.isEmpty()) {
                sendErrorMessage()
            } else {
                var userData = userList[0]
                var decryptedPassword = decrypt(userList[0].userMPinNumber, userList[0].userIVValue, userList[0].userCipherKey)
                if(decryptedPassword == enteredPassword ) {
                    signInToVault()
                    var sharedPreferences = context.getSharedPreferences("MyPassPrefs", 0)
                    var editor = sharedPreferences.edit()
                    editor.putString("userMobileNumber", userData.userMobileNumber)
                    editor.apply()
                    val intent = Intent(context, PassHomeActivity::class.java)
                    //intent.putExtra("userMobileNumber", userList[0].userMobileNumber)
                    context.startActivity(intent)


                } else {
                    wrongPasswordMessage()
                }
            }

        }
    }

    private fun sendErrorMessage() {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val customLayoutToast: View = inflater.inflate(R.layout.custom_toast_message, null)
        (customLayoutToast.findViewById<View>(R.id.tvToastMessageOne) as TextView).text = context.getString(
                    R.string.user_not_found)
        //show toast
        val customToast = Toast(context)
        customToast.view = customLayoutToast
        customToast.duration = Toast.LENGTH_SHORT
        customToast.show()
    }
    private fun signInToVault() {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val customLayoutToast: View = inflater.inflate(R.layout.custom_toast, null)
        (customLayoutToast.findViewById<View>(R.id.tvCTToastMessageOne) as TextView).text = context.getString(
                    R.string.congrats_success)
        (customLayoutToast.findViewById<View>(R.id.tvCTToastMessageTwo) as TextView).text = context.getString(
                    R.string.sign_in_to_vault)

        //show toast
        val customToast = Toast(context)
        customToast.view = customLayoutToast
        customToast.duration = Toast.LENGTH_SHORT
        customToast.show()
    }
    private fun wrongPasswordMessage() {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val customLayoutToast: View = inflater.inflate(R.layout.custom_toast_message, null)
        (customLayoutToast.findViewById<View>(R.id.tvToastMessageOne) as TextView).text = context.getString(
                    R.string.wrong_password)
        //show toast
        val customToast = Toast(context)
        customToast.view = customLayoutToast
        customToast.duration = Toast.LENGTH_SHORT
        customToast.show()
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        //
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
      //
    }


}