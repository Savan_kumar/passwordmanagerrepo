/*
 * *****************************************************************************
 * Utils.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 30/9/20 3:37 PM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.helper

import android.os.Build
import java.util.*
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class Utils{

}
fun String.isValidMobile(): Boolean {
    return this.length == 10
}

fun String.isValidMpin(): Boolean {
    return this.length == 4
}


data class CipherText(val cipherText: ByteArray, val iv: ByteArray, val key: String)



fun String.encrypt(): CipherText {
    val charset = Charsets.UTF_8
    val plaintext: ByteArray = this.toByteArray(charset)
    val keygen = KeyGenerator.getInstance("AES")
    keygen.init(256)
    val key = keygen.generateKey()
    val encodedKey: String = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        Base64.getEncoder().encodeToString(key.encoded)
    } else {
        android.util.Base64.encodeToString(key.encoded, android.util.Base64.DEFAULT);
    }
    val cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING")
    cipher.init(Cipher.ENCRYPT_MODE, key)
    val ciphertext: ByteArray = cipher.doFinal(plaintext)

    val iv = cipher.iv
    //decrypt(ciphertext)
    return CipherText(ciphertext, iv, encodedKey)

}

fun decrypt(ciphertext: ByteArray, ivToDecrypt: ByteArray, encodedKey: String): String {
    val charset = Charsets.UTF_8
    val encryptedText: ByteArray = ciphertext
    val keygen = KeyGenerator.getInstance("AES")
    keygen.init(256)
    val cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING")
    // val key: SecretKey = keygen.generateKey()
    // decode the base64 encoded string
    val decodedKey = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        Base64.getDecoder().decode(encodedKey)
    } else {
        android.util.Base64.decode(encodedKey, android.util.Base64.DEFAULT)
    }
    // rebuild key using SecretKeySpec
    val originalKey: SecretKey = SecretKeySpec(decodedKey, 0, decodedKey.size, "AES")
    val iv = IvParameterSpec(ivToDecrypt)
    cipher.init(Cipher.DECRYPT_MODE, originalKey, iv)
    val original: ByteArray = cipher.doFinal(encryptedText)
    return original.toString(charset)
}