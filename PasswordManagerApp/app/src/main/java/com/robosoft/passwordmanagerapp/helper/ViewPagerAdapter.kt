/*
 * *****************************************************************************
 * ViewPagerAdapter.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 30/9/20 1:59 AM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.helper
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.robosoft.passwordmanagerapp.fragment.SignInFragment
import com.robosoft.passwordmanagerapp.fragment.SignUpFragment

class ViewPagerAdapter( fragment: FragmentManager, totalTabs: Int): FragmentPagerAdapter(fragment, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private lateinit var fragment: Fragment
    private val totalTabs = totalTabs
    private lateinit var title: String
    override fun getItem(position: Int): Fragment {
        when (position){
            0 -> fragment = SignInFragment()
            1 -> fragment = SignUpFragment()

        }
        return fragment
    }

    override fun getCount(): Int {
        return totalTabs
    }


}