/*
 * *****************************************************************************
 * PassHomeViewModel.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 30/9/20 11:26 PM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.viewmodel

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.Observable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.robosoft.passwordmanagerapp.model.SiteDetailClass
import com.robosoft.passwordmanagerapp.model.database.UserDataRepository
import com.robosoft.passwordmanagerapp.view.AddSiteActivity

class PassHomeViewModel( val dataRepository: UserDataRepository) : ViewModel(), Observable {

    val siteDataList = MutableLiveData<List<SiteDetailClass>>()
    val addSiteButtonClicked = MutableLiveData<Boolean>()
    var userMobileNumber: String = ""
    val onInitialExecution = MutableLiveData<Boolean>()
    init {
        onInitialExecution.value = true

    }

    fun getDataList() {
        siteDataList.value = dataRepository.getSiteByMobileNumber(userMobileNumber)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        //
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        //
    }
}