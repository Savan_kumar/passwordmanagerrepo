/*
 * *****************************************************************************
 * SignUpViewModel.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 30/9/20 2:57 AM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.viewmodel

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.robosoft.passwordmanagerapp.R
import com.robosoft.passwordmanagerapp.helper.encrypt
import com.robosoft.passwordmanagerapp.model.UserClass
import com.robosoft.passwordmanagerapp.model.database.UserDataRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class SignUpViewModelClass( val dataRepository: UserDataRepository) : ViewModel(), Observable{
    lateinit var userList: List<UserClass>
    lateinit var context: Context
    var dataEntry: Long? = null
    @Bindable
    val userMobileNumber = MutableLiveData<String>()
    @Bindable
    val userMPin = MutableLiveData<String>()
    @Bindable
    val userConfirmMPin = MutableLiveData<String>()
    @Bindable
    val isValidMobileNumber = MutableLiveData<Boolean>()
    @Bindable
    val isValidMPin = MutableLiveData<Boolean>()
    @Bindable
    val isMobileNumberEmpty = MutableLiveData<Boolean>()
    @Bindable
    val isMPinEmpty = MutableLiveData<Boolean>()
    @Bindable
    val isConfirmMPinValid = MutableLiveData<Boolean>()
    @Bindable
    val isConfirmMPinEmpty = MutableLiveData<Boolean>()
    @Bindable
    val isMPinMatching = MutableLiveData<Boolean>()

    fun signUpUser(view: View) {
        context = view.context
        if(userMobileNumber.value == null){
            isMobileNumberEmpty.value = true
        } else if(userMobileNumber.value!!.length!=10) {
            isValidMobileNumber.value = true
        }
        else if(userMPin.value == null) {
            isMPinEmpty.value = true
        } else if(userMPin.value!!.length!=4) {
            isValidMPin.value = true
        }
        else if(userConfirmMPin.value == null){
            isConfirmMPinEmpty.value = true
        }
        else if(userConfirmMPin.value!!.length != 4){
            isConfirmMPinValid.value = true
        }
        else if(userConfirmMPin.value!! != userMPin.value!!){
            isMPinMatching.value = true
        }
        else {
            //add user detail from db
            userList = dataRepository.getUserByPhone(userMobileNumber.value!!)
            if(userList.isEmpty()) {
                addUserData()
            } else {
                sendUserExistsError()
            }

        }
    }

    private fun addUserData() {
        val cipherValues = userMPin.value!!.encrypt()
        insertData(UserClass(userMobileNumber.value!!, cipherValues.cipherText, cipherValues.iv, cipherValues.key))



    }
    private fun sendUserExistsError() {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val customLayoutToast: View = inflater.inflate(R.layout.custom_toast_message, null)
        (customLayoutToast.findViewById<View>(R.id.tvToastMessageOne) as TextView).text = context.getString(R.string.user_already_exists)
        //show toast
        val customToast = Toast(context)
        customToast.view = customLayoutToast
        customToast.duration = Toast.LENGTH_LONG
        customToast.show()

    }
    private fun sendInsertDataError() {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val customLayoutToast: View = inflater.inflate(R.layout.custom_toast_message, null)
        (customLayoutToast.findViewById<View>(R.id.tvToastMessageOne) as TextView).text = context.getString(
                    R.string.error_inserting_data)
        //show toast
        val customToast = Toast(context)
        customToast.view = customLayoutToast
        customToast.duration = Toast.LENGTH_LONG
        customToast.show()
    }
    private fun sendSuccessMessage() {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val customLayoutToast: View = inflater.inflate(R.layout.custom_toast_message, null)
        (customLayoutToast.findViewById<View>(R.id.tvToastMessageOne) as TextView).text = context.getString(
                    R.string.user_added_success)
        //show toast
        val customToast = Toast(context)
        customToast.view = customLayoutToast
        customToast.duration = Toast.LENGTH_LONG
        customToast.show()
    }
    private fun insertData(userClass: UserClass) : Job = viewModelScope.launch {

        dataEntry = dataRepository.insertUserData(userClass)
        if (dataEntry == null) {
            sendInsertDataError()
        } else {
            sendSuccessMessage()
        }

    }




    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        //
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
      //
    }

}