/*
 * *****************************************************************************
 * PassManagerDatabase.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 30/9/20 3:23 PM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.model.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

import com.robosoft.passwordmanagerapp.model.SiteDetailClass
import com.robosoft.passwordmanagerapp.model.UserClass

@Database(entities = [UserClass::class, SiteDetailClass::class], version = 1, exportSchema = false)
abstract class PassManagerDatabase: RoomDatabase() {

    abstract val myUserDAO: MyUserDAO
    abstract val myUserSiteDao: MyUserSiteDao

    companion object{
        @Volatile
        private var INSTANCE: PassManagerDatabase? = null
        fun getInstance(context: Context): PassManagerDatabase {
            synchronized(this) {
                var instance: PassManagerDatabase? = INSTANCE
                if(instance==null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        PassManagerDatabase::class.java,
                        "pass_manager_database"
                    ).allowMainThreadQueries()
                        .build()
                }
                return instance
            }
        }
    }

}
