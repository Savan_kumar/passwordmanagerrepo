/*
 * *****************************************************************************
 * UserClass.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 30/9/20 2:06 PM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_data_table")
data class UserClass (

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "user_mobile_number") val userMobileNumber: String,

    @ColumnInfo(name = "user_m_pin_number") val userMPinNumber: ByteArray,

    @ColumnInfo(name = "user_iv_value") val userIVValue: ByteArray,

    @ColumnInfo(name = "user_cipher_keys") val userCipherKey: String

)