/*
 * *****************************************************************************
 * SignInFragment.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 30/9/20 1:52 AM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.fragment
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.robosoft.passwordmanagerapp.R
import com.robosoft.passwordmanagerapp.databinding.FragmentSignInBinding
import com.robosoft.passwordmanagerapp.model.database.*
import com.robosoft.passwordmanagerapp.viewmodel.SignInViewModelClass


class SignInFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
      // inflater.inflate(R.layout.fragment_sign_in, container, false)
        val context = context!!.applicationContext
        val userDao: MyUserDAO = PassManagerDatabase.getInstance(context).myUserDAO
        val siteDao: MyUserSiteDao = PassManagerDatabase.getInstance(context).myUserSiteDao
        val factory = PassManagerFactory(UserDataRepository(userDao, siteDao))
        val binding = DataBindingUtil.inflate<FragmentSignInBinding>(inflater, R.layout.fragment_sign_in, container, false)
        val signInViewModel = ViewModelProvider(this, factory).get(SignInViewModelClass::class.java)
        binding.lifecycleOwner = this
        binding.signInViewModel = signInViewModel


        val icon = resources.getDrawable(R.drawable.base_line_error)
        icon?.setBounds(0, 0, icon.intrinsicWidth, icon.intrinsicHeight)

       signInViewModel.isMobileNumberEmpty.observe(this, Observer {
            if(it) {
                binding.editMobileNumber.setError("Mobile Number cannot be empty", icon)
                binding.editMobileNumber.requestFocus()
            }
        })

        signInViewModel.isValidMobileNumber.observe(this, Observer {
            if(it) {
                binding.editMobileNumber.setError("Enter valid Mobile Number", icon)
                binding.editMobileNumber.requestFocus()
            }
        })
        signInViewModel.isMPinEmpty.observe(this, Observer {
            if(it) {
                binding.editMPin.setError("MPin cannot be empty", icon)
                binding.editMPin.requestFocus()
            }
        })
        signInViewModel.isValidMPin.observe(this, Observer {
            if(it) {
                binding.editMPin.setError("Enter Valid MPin", icon)
                binding.editMPin.requestFocus()
            }
        })
        binding.passToggleCheckbox.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked) {
                binding.editMPin.transformationMethod = HideReturnsTransformationMethod.getInstance()
            } else {
                binding.editMPin.transformationMethod = PasswordTransformationMethod.getInstance()
            }
        }

        return binding.root
    }


}