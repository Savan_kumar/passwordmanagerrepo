/*
 * *****************************************************************************
 * MyUserSiteDao.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 30/9/20 3:21 PM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.model.database

import androidx.room.*
import com.robosoft.passwordmanagerapp.model.SiteDetailClass

@Dao
interface MyUserSiteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun  insertUserSiteData(siteDetailClass: SiteDetailClass) : Long

    @Query("SELECT * FROM user_site_detail WHERE user_mobile_number =:userPhone")
    fun getUserSite(userPhone: String) : List<SiteDetailClass>

    @Query("SELECT * FROM user_site_detail WHERE site_id =:siteID")
    fun getSiteByID(siteID: Int): List<SiteDetailClass>

    @Query("DELETE FROM user_site_detail WHERE site_id=:siteID AND user_mobile_number=:userPhone")
    suspend fun deleteSiteByID(siteID: Int, userPhone: String)


}