/*
 * *****************************************************************************
 * AuthenticationActivity.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 30/9/20 1:03 AM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.view

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.google.android.material.tabs.TabLayout
import com.robosoft.passwordmanagerapp.R
import com.robosoft.passwordmanagerapp.helper.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_authentication.*

class AuthenticationActivity : AppCompatActivity() {
    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)

        tabLayout.addTab(tabLayout.newTab().setText("SIGN IN"))
        tabLayout.addTab(tabLayout.newTab().setText("SIGN UP"))
        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(applicationContext, R.color.colorTabIndicator))
        tabLayout.setSelectedTabIndicator(R.drawable.tab_indicator)

        val adapter = ViewPagerAdapter(supportFragmentManager, tabLayout.tabCount)
        authenticationViewPager.adapter = adapter
        authenticationViewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabReselected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {

            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                authenticationViewPager.currentItem = tabLayout.selectedTabPosition
            }
        })
    }
}