/*
 * *****************************************************************************
 * EditSiteActivity.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 2/10/20 1:34 AM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.view

import android.content.res.ColorStateList
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.KeyListener
import android.text.method.PasswordTransformationMethod
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.View.FOCUSABLE
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import androidx.core.view.isInvisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.robosoft.passwordmanagerapp.R
import com.robosoft.passwordmanagerapp.databinding.ActivityEditSiteBinding
import com.robosoft.passwordmanagerapp.model.database.*
import com.robosoft.passwordmanagerapp.viewmodel.EditSiteViewModelClass
import kotlinx.android.synthetic.main.activity_edit_site.*
import kotlinx.android.synthetic.main.activity_pass_home.*

class EditSiteActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    lateinit var binding: ActivityEditSiteBinding
    lateinit var editSiteViewModel: EditSiteViewModelClass
    var menuState = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // setContentView(R.layout.activity_edit_site)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_site)
        setUpToolBar()
        val context = this
        val userDao: MyUserDAO = PassManagerDatabase.getInstance(context).myUserDAO
        val siteDao: MyUserSiteDao = PassManagerDatabase.getInstance(context).myUserSiteDao
        val factory = PassManagerFactory(UserDataRepository(userDao, siteDao))
        editSiteViewModel = ViewModelProvider(this, factory).get(EditSiteViewModelClass::class.java)
        binding.lifecycleOwner = this
        binding.editSiteViewModel = editSiteViewModel

        var intentValue = intent
        var userMobileNumber =  intentValue.getStringExtra("userMobileNumber")
        var siteID = intentValue.getIntExtra("siteID", 0)

        val icon = resources.getDrawable(R.drawable.base_line_error)
        icon?.setBounds(
            0, 0,
            icon.intrinsicWidth,
            icon.intrinsicHeight
        )

        editSiteViewModel.onInitialExecution.observe(this, Observer {
            if(it) {
                editSiteViewModel.getSiteData(siteID, userMobileNumber)
            }
        })
        editSiteViewModel.isUrlEmpty.observe(this, Observer {
            if(it) {
                binding.etURLValue.setError("URL Cannot be blank", icon)
                binding.etURLValue.requestFocus()
            }
        })
        editSiteViewModel.isSiteNameEmpty.observe(this, Observer {
            if(it) {
                binding.etSiteNameValue.setError("Site Name cannot be blank", icon)
                binding.etSiteNameValue.requestFocus()
            }
        })
        editSiteViewModel.isSiteTypeEmpty.observe(this, Observer {
            if(it) {
                binding.etSectorFolderValue.setError("Select Site type", icon)
                binding.etSectorFolderValue.requestFocus()
            }
        })
        editSiteViewModel.isSiteUserNameEmpty.observe(this, Observer {
            if(it) {
                binding.etUserNameValue.setError("User name cannot be blank", icon)
                binding.etUserNameValue.requestFocus()
            }
        })
        editSiteViewModel.isSiteUserPasswordEmpty.observe(this, Observer {
            if(it) {
                binding.etUserPasswordValue.setError("User password cannot be blank", icon)
                binding.etUserPasswordValue.requestFocus()
            }
        })
        editSiteViewModel.siteUserPasswordValue.observe(this, Observer {
            when {
                it == null -> editSiteViewModel.progressRate.value = 0
                it.length <= 6 -> editSiteViewModel.progressRate.value = 25
                it.length <= 12 -> editSiteViewModel.progressRate.value = 50
                it.length > 12 -> editSiteViewModel.progressRate.value = 100
            }
        })
        editSiteViewModel.progressRate.observe(this, Observer {
            if(it == 0 || it == 25) binding.editProgressBar.progressTintList = ColorStateList.valueOf(resources.getColor(R.color.weakPassword))
            else if(it == 50)  binding.editProgressBar.progressTintList = ColorStateList.valueOf(resources.getColor(R.color.intermediatePassword))
            else if(it ==100) binding.editProgressBar.progressTintList = ColorStateList.valueOf(resources.getColor(R.color.strongPassword))
        })
        editSiteViewModel.afterUpdate.observe(this, Observer {
            if(it) {
                finish()
            }
        })
        binding.passToggleEditSite.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked) {
                binding.etUserPasswordValue.transformationMethod = HideReturnsTransformationMethod.getInstance()
            } else {
                binding.etUserPasswordValue.transformationMethod = PasswordTransformationMethod.getInstance()
            }
        }



    }


    private fun setUpToolBar() {
        supportActionBar?.title = "Site Details"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.edit_menu, menu)
        if(menuState == "Hidden") menu?.getItem(0)?.isVisible = false
         else {
            binding.etURLValue.keyListener = null
            binding.etSiteNameValue.keyListener = null
            binding.etUserNameValue.keyListener = null
            binding.etUserPasswordValue.keyListener = null
            binding.etUserNotesValue.keyListener = null
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.action_edit -> {
                supportActionBar?.title = "Edit"
                menuState = "Hidden"
                //start edit site activity
                binding.btnUpdate.visibility = View.VISIBLE
                setUpSpinner()
                binding.etURLValue.keyListener = EditText(applicationContext).keyListener
                binding.etSiteNameValue.keyListener = EditText(applicationContext).keyListener
                binding.etUserNameValue.keyListener = EditText(applicationContext).keyListener
                binding.etUserPasswordValue.keyListener = EditText(applicationContext).keyListener
                binding.etUserNotesValue.keyListener = EditText(applicationContext).keyListener
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun setUpSpinner(){
        val siteTypes = resources.getStringArray(R.array.site_types)
        val spinnerAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, siteTypes)
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerViewEditSite.adapter = spinnerAdapter
        spinnerViewEditSite.onItemSelectedListener = this

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        var textFromSpinner: String = parent?.getItemAtPosition(position).toString()
        binding.etSectorFolderValue.text = textFromSpinner

    }
}