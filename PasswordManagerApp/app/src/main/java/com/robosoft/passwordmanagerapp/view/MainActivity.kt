/*
 * *****************************************************************************
 * MainActivity.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 30/9/20 1:01 AM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.view
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.robosoft.passwordmanagerapp.R

class MainActivity : AppCompatActivity() {
    private val handler = Handler()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)


    }
    private val runnable = Runnable {
        if(!isFinishing) {
            var intent = Intent(this, AuthenticationActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        handler.postDelayed(runnable, 3000)
    }

    override fun onPause() {
        super.onPause()
        handler.removeCallbacks(runnable)
    }
}