/*
 * *****************************************************************************
 * PassHomeActivity.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 30/9/20 11:25 PM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import androidx.recyclerview.widget.LinearLayoutManager
import com.robosoft.passwordmanagerapp.R
import com.robosoft.passwordmanagerapp.databinding.ActivityAddSiteBinding
import com.robosoft.passwordmanagerapp.databinding.ActivityPassHomeBinding
import com.robosoft.passwordmanagerapp.helper.RecyclerAdapterClass
import com.robosoft.passwordmanagerapp.model.SiteDetailClass
import com.robosoft.passwordmanagerapp.model.database.*
import com.robosoft.passwordmanagerapp.viewmodel.AddSiteViewModelClass
import com.robosoft.passwordmanagerapp.viewmodel.PassHomeViewModel
import kotlinx.android.synthetic.main.activity_pass_home.*
import kotlinx.android.synthetic.main.fragment_sign_in.*
import kotlinx.android.synthetic.main.recyclerview_item_layout.*

class PassHomeActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    lateinit var binding: ActivityPassHomeBinding
    private lateinit var passHomeViewModel: PassHomeViewModel
    private var adapter = RecyclerAdapterClass()
    var siteList = ArrayList<SiteDetailClass>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pass_home)

        setUpActionBar()
        var sharedPreferences = getSharedPreferences("MyPassPrefs", 0)
        val context = this
        val userDao: MyUserDAO = PassManagerDatabase.getInstance(context).myUserDAO
        val siteDao: MyUserSiteDao = PassManagerDatabase.getInstance(context).myUserSiteDao
        val factory = PassManagerFactory(UserDataRepository(userDao, siteDao))
        passHomeViewModel = ViewModelProvider(this, factory).get(PassHomeViewModel::class.java)
        binding.lifecycleOwner = this
        binding.passHomeViewModel = passHomeViewModel
        //var intentValue = intent
        //var userMobileNumber: String =  intentValue.getStringExtra("userMobileNumber")
        var userMobileNumber = sharedPreferences.getString("userMobileNumber", "")

        setUpSpinner()

        passHomeViewModel.onInitialExecution.observe(this, Observer {
            if(it) {
                if (userMobileNumber != null) {
                    passHomeViewModel.userMobileNumber = userMobileNumber
                }
                passHomeViewModel.getDataList()
            }

        })
        passHomeViewModel.siteDataList.observe(this, Observer {
            Log.d("myTagList", it.toString())
            initRecyclerView()
            siteList = it as ArrayList<SiteDetailClass>
            adapter.siteList = siteList
            binding.tvSiteItemCounter.text = adapter.siteList.size.toString()
            adapter.notifyDataSetChanged()
        })
        fabAddSite.setOnClickListener {
            var intent = Intent(context, AddSiteActivity::class.java)
            //intent.putExtra("userMobileNumber", userMobileNumber)
            startActivity(intent)
        }



    }

    private fun initRecyclerView() {
        siteRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        siteRecyclerView.adapter = adapter
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.home_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.profile_view -> showProfilePopup()
           R.id.search -> showSearchBar()
            R.id.sync -> {
                val image = ImageView(this)
                image.setImageResource(R.drawable.icon_syncing)
                val dialog = AlertDialog.Builder(this, R.style.AlertDialogCustom)
                    .setTitle("")
                    .setMessage("Data sync in progress\n" +
                            "please wait")
                    .setView(image)
                    .show()
                val textView =  dialog.findViewById<TextView>(android.R.id.message)
                textView?.setTextSize(20F)
                val lp = dialog.window!!.attributes
                lp.dimAmount = 0.9f
                dialog.window!!.attributes = lp
                dialog.window!!.addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setUpActionBar() {
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setLogo(R.drawable.action_bar_logo);
        supportActionBar?.setDisplayUseLogoEnabled(true);
        supportActionBar?.setHomeAsUpIndicator(R.drawable.burger_menu)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.elevation = 20F
        supportActionBar?.title = ""
    }


    private fun setUpSpinner(){
        val siteTypes = resources.getStringArray(R.array.site_types_home)
        val spinnerAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, siteTypes)
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerView.adapter = spinnerAdapter
        spinnerView.onItemSelectedListener = this

    }
    private fun showProfilePopup() {
        val menuItemView: View = findViewById(R.id.profile_view)
        val wrapper: Context = ContextThemeWrapper(this, R.style.PopupMenu)
        val popupMenu = PopupMenu(wrapper, menuItemView)
        popupMenu.setOnMenuItemClickListener {
            when(it.itemId) {
                R.id.action_logout -> {
                    startActivity(Intent(this, AuthenticationActivity::class.java))
                    
                    true
                } else -> false
            }
        }
        popupMenu.inflate(R.menu.profile_menu)
        try {
            val fieldMPopup = PopupMenu::class.java.getDeclaredField("mPopup")
            fieldMPopup.isAccessible = true
            val mPopup = fieldMPopup.get(popupMenu)
            mPopup.javaClass
                .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                .invoke(mPopup, true)
        } catch (e: Exception) {

        } finally {
            popupMenu.show()
        }
    }
    @SuppressLint("ClickableViewAccessibility")
    private fun showSearchBar() {
        val mySearchEditText = EditText(this)
        mySearchEditText.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT , 180)
        mySearchEditText.background = resources.getDrawable(R.drawable.searchview_background, null)
        mySearchEditText.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, resources.getDrawable(R.drawable.ic_baseline_arrow_forward_24, null), null)
        mySearchEditText.hint = getString(R.string.search_hint_text)
        mySearchEditText.setPadding(50, 24, 50, 24)
        mySearchEditText.setHintTextColor(resources.getColor(R.color.hintColor, null))
        constraintLayout.addView(mySearchEditText)
        mySearchEditText.setOnTouchListener(object: View.OnTouchListener{
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                if(event?.x!! >= constraintLayout.width - 100 ) {
                    constraintLayout.removeView(mySearchEditText)
                    //filter data

                    return true;
                }
                return false
            }
        })

        mySearchEditText.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                //
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // filterdata
                var searchText = s.toString()
                val filteredList = ArrayList<SiteDetailClass>()
                if(searchText.length > 1) {
                    for (item in siteList)
                    {

                        if(item.siteName.toLowerCase().contains(searchText.toLowerCase())) {
                            filteredList.add(item)
                        }

                    }
                    adapter.siteList = filteredList
                    binding.tvSiteItemCounter.text = adapter.siteList.size.toString()
                    adapter.notifyDataSetChanged()
                } else {
                    adapter.siteList = siteList
                    binding.tvSiteItemCounter.text = adapter.siteList.size.toString()
                    adapter.notifyDataSetChanged()
                }

            }
        })

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        //
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        var textFromSpinner: String = parent?.getItemAtPosition(position).toString()
        binding.tvSpinnerSelectedValue.text = textFromSpinner
        //filterdata

        val filteredList = ArrayList<SiteDetailClass>()
        for (item in siteList)
        {
                if(textFromSpinner == "All") {
                    filteredList.add(item)
                }
                 else if (item.siteType.toLowerCase().contains(textFromSpinner.toLowerCase())) {
                    filteredList.add(item)
                }
        }

        adapter.filterListData(filteredList)
        binding.tvSiteItemCounter.text =  adapter.siteList.size.toString()


    }
}