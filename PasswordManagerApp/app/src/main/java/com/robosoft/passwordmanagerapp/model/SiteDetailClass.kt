/*
 * *****************************************************************************
 * SiteDetailClass.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 30/9/20 2:51 PM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_site_detail")
data class SiteDetailClass (

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "site_id") val siteID: Int,

    @ColumnInfo(name = "user_mobile_number") val userMobileNumber: String,

    @ColumnInfo(name = "site_url") var siteURL: String,

    @ColumnInfo(name = "site_name") var siteName: String,

    @ColumnInfo(name = "site_Type") var siteType: String,

    @ColumnInfo(name = "site_user_name") var siteUserName: String,

    @ColumnInfo(name = "site_user_password") var siteUserPassword: ByteArray,

    @ColumnInfo(name = "site_notes") var siteNotes: String,

    @ColumnInfo(name = "site_iv_value") var siteIVValue: ByteArray,

    @ColumnInfo(name = "site_cipher_key") var siteCipherKey: String

)