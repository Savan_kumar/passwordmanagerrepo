/*
 * *****************************************************************************
 * MyUserDAO.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 30/9/20 3:18 PM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.model.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.robosoft.passwordmanagerapp.model.UserClass

@Dao
interface MyUserDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(userClass: UserClass) : Long

    @Query("SELECT * FROM user_data_table WHERE user_mobile_number =:phoneNumber")
    fun getUser(phoneNumber: String): List<UserClass>

    



}