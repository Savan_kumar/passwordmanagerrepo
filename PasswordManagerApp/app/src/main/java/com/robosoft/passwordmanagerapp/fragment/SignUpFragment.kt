/*
 * *****************************************************************************
 * SignUpFragment.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 30/9/20 1:51 AM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.robosoft.passwordmanagerapp.R
import com.robosoft.passwordmanagerapp.databinding.FragmentSignUpBinding
import com.robosoft.passwordmanagerapp.model.database.*
import com.robosoft.passwordmanagerapp.viewmodel.SignUpViewModelClass
import kotlinx.android.synthetic.main.fragment_sign_up.*

class SignUpFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val context = context!!.applicationContext
        val userDao: MyUserDAO = PassManagerDatabase.getInstance(context).myUserDAO
        val siteDao: MyUserSiteDao = PassManagerDatabase.getInstance(context).myUserSiteDao
        val factory = PassManagerFactory(UserDataRepository(userDao, siteDao))
        val binding = DataBindingUtil.inflate<FragmentSignUpBinding>(inflater, R.layout.fragment_sign_up, container, false)
        val signUpViewModel = ViewModelProvider(this, factory).get(SignUpViewModelClass::class.java)
        binding.lifecycleOwner = this
        binding.signUpViewModel = signUpViewModel

        val icon = resources.getDrawable(R.drawable.base_line_error)
        icon?.setBounds(0, 0, icon.intrinsicWidth, icon.intrinsicHeight)


        signUpViewModel.isMobileNumberEmpty.observe(this, Observer {
            if(it) {
                binding.tvSignUpMobileNumber.setError("Mobile Number cannot be empty", icon)
                binding.tvSignUpMobileNumber.requestFocus()
            }
        })
        signUpViewModel.isValidMobileNumber.observe(this, Observer {
            if(it) {
                binding.tvSignUpMobileNumber.setError("Enter valid Mobile Number", icon)
                binding.tvSignUpMobileNumber.requestFocus()
            }
        })
        signUpViewModel.isMPinEmpty.observe(this, Observer {
            if(it) {
                binding.tvSignUpMPin.setError("MPin Cannot be Empty", icon)
                //Toast.makeText(context, "MPin Cannot be Empty", Toast.LENGTH_SHORT).show()
                binding.tvSignUpMPin.text = null
                binding.tvSignUpMPin.requestFocus()
            }
        })
        signUpViewModel.isValidMPin.observe(this, Observer {
            if(it) {
                binding.tvSignUpMPin.setError("Enter Valid MPin", icon)
                //Toast.makeText(context, "Enter Valid MPin", Toast.LENGTH_SHORT).show()
                binding.tvSignUpMPin.text = null
                binding.tvSignUpMPin.requestFocus()
            }
        })
        signUpViewModel.isConfirmMPinEmpty.observe(this, Observer {
            if(it) {
                binding.tvSignUpMPinConfirm.setError("Confirm MPin cannot be Empty", icon)
                //Toast.makeText(context, "Confirm MPin cannot be Empty", Toast.LENGTH_SHORT).show()
                binding.tvSignUpMPinConfirm.text = null
                binding.tvSignUpMPinConfirm.requestFocus()
            }
        })

        signUpViewModel.isConfirmMPinValid.observe(this, Observer {
            if(it) {
                binding.tvSignUpMPinConfirm.setError("Enter valid MPin", icon)
                //Toast.makeText(context, "Enter valid MPin", Toast.LENGTH_SHORT).show()
                binding.tvSignUpMPinConfirm.text = null
                binding.tvSignUpMPinConfirm.requestFocus()
            }
        })
        signUpViewModel.isMPinMatching.observe(this, Observer {
            if(it) {
                binding.tvSignUpMPinConfirm.setError("MPin does not match", icon)
                //Toast.makeText(context, "MPin does not match", Toast.LENGTH_SHORT).show()
                binding.tvSignUpMPinConfirm.requestFocus()
            }
        })

        binding.passToggleSignUp.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked) {
                binding.tvSignUpMPinConfirm.transformationMethod = HideReturnsTransformationMethod.getInstance()
            } else {
                binding.tvSignUpMPinConfirm.transformationMethod = PasswordTransformationMethod.getInstance()
            }
        }
        return binding.root
    }


}