/*
 * *****************************************************************************
 * EditSiteViewModelClass.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 2/10/20 12:19 PM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.viewmodel

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.Observable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.robosoft.passwordmanagerapp.R
import com.robosoft.passwordmanagerapp.helper.CipherText
import com.robosoft.passwordmanagerapp.helper.decrypt
import com.robosoft.passwordmanagerapp.helper.encrypt
import com.robosoft.passwordmanagerapp.model.SiteDetailClass
import com.robosoft.passwordmanagerapp.model.database.UserDataRepository
import com.robosoft.passwordmanagerapp.view.PassHomeActivity
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class EditSiteViewModelClass( val dataRepository: UserDataRepository) : ViewModel(), Observable {

    lateinit var context: Context
    val siteUrlValue = MutableLiveData<String>()
    val siteNameValue = MutableLiveData<String>()
    val siteTypeValue = MutableLiveData<String>()
    val siteUserNameValue = MutableLiveData<String>()
    val siteUserPasswordValue = MutableLiveData<String>()
    val siteNotesValue = MutableLiveData<String>()
    val sitePasswordByteArray = MutableLiveData<ByteArray>()
    val siteIVKey = MutableLiveData<ByteArray>()
    val siteCipherKey = MutableLiveData<String>()
    val progressRate = MutableLiveData<Int>()
    var userMobileNumber = ""
    var dataEntry : Long = 0
    val isUrlEmpty = MutableLiveData<Boolean>()
    val isSiteNameEmpty = MutableLiveData<Boolean>()
    val isSiteTypeEmpty = MutableLiveData<Boolean>()
    val isSiteUserNameEmpty = MutableLiveData<Boolean>()
    val isSiteUserPasswordEmpty = MutableLiveData<Boolean>()
    val onInitialExecution = MutableLiveData<Boolean>()
    var siteID: Int = 0
    var dataHolder = ArrayList<SiteDetailClass>()
    val afterUpdate = MutableLiveData<Boolean>()
    init {
        onInitialExecution.value = true
        siteNotesValue.value = ""

    }

    fun getSiteData(siteID: Int, userMobileNumber: String) {
        this.userMobileNumber = userMobileNumber
        this.siteID = siteID

        dataHolder = dataRepository.getSiteByID(siteID) as ArrayList<SiteDetailClass>
        if(dataHolder.isEmpty()) {
            Toast.makeText(context, "Error in retrieving data", Toast.LENGTH_LONG).show()
        } else {

            siteUrlValue.value = dataHolder[0].siteURL
            siteNameValue.value = dataHolder[0].siteName
            siteTypeValue.value = dataHolder[0].siteType
            siteUserNameValue.value = dataHolder[0].siteUserName
            siteUserPasswordValue.value = decrypt(dataHolder[0].siteUserPassword, dataHolder[0].siteIVValue, dataHolder[0].siteCipherKey)
            siteNotesValue.value = dataHolder[0].siteNotes

        }
    }
    fun updateBtnClicked(view: View) {
        context = view.context
        var result = checkDataFields()

        var cipherValues = siteUserPasswordValue.value!!.encrypt()
        if(result) {
                addSiteData(cipherValues)
        } else {
                //error message
                errorUpdatingData()
            }
        }

    fun addSiteData(cipherText: CipherText) {
        sitePasswordByteArray.value = cipherText.cipherText
        siteIVKey.value = cipherText.iv
        siteCipherKey.value = cipherText.key
        insertSiteData(SiteDetailClass(siteID, userMobileNumber, siteUrlValue.value!!, siteNameValue.value!!, siteTypeValue.value!!, siteUserNameValue.value!!,sitePasswordByteArray.value!!, siteNotesValue.value!!, siteIVKey.value!!, siteCipherKey.value!! ))
    }
    private fun insertSiteData(siteDetailClass: SiteDetailClass): Job = viewModelScope.launch {
        dataEntry = dataRepository.insertSiteData(siteDetailClass)
        if(dataEntry == null) {
            errorUpdatingData()
        } else {
            updateSiteSuccess()
            val intent = Intent(context, PassHomeActivity::class.java)
            context.startActivity(intent)
            afterUpdate.value = true

        }
    }
    private fun errorUpdatingData() {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val customLayoutToast: View = inflater.inflate(R.layout.custom_toast_message, null)
        (customLayoutToast.findViewById<View>(R.id.tvToastMessageOne) as TextView).text = context.getString(
                    R.string.error_update_message)

        //show toast
        val customToast = Toast(context)
        customToast.view = customLayoutToast
        customToast.duration = Toast.LENGTH_LONG
        customToast.show()
    }
    private fun updateSiteSuccess() {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val customLayoutToast: View = inflater.inflate(R.layout.custom_toast_message, null)
        (customLayoutToast.findViewById<View>(R.id.tvToastMessageOne) as TextView).text = context.getString(
                    R.string.update_success_message)

        //show toast
        val customToast = Toast(context)
        customToast.view = customLayoutToast
        customToast.duration = Toast.LENGTH_LONG
        customToast.show()
    }
    private fun checkDataFields(): Boolean {
        when {
            siteUrlValue.value == null -> {
                isUrlEmpty.value = true
                return false
            }
            siteNameValue.value == null -> {
                isSiteNameEmpty.value = true
                return false
            }
            siteTypeValue.value == null -> {
                isSiteTypeEmpty.value = true
                return false
            }
            siteUserNameValue.value == null -> {
                isSiteUserNameEmpty.value = true
                return false
            }
            siteUserPasswordValue.value == null -> {
                isSiteUserPasswordEmpty.value = true
            }
            else -> {
                return true
            }
        }
        return true
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
       //
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        //
    }
}