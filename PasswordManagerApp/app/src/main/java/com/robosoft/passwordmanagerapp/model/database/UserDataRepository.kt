/*
 * *****************************************************************************
 * UserDataRepository.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 30/9/20 3:22 PM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.model.database

import android.service.autofill.UserData
import com.robosoft.passwordmanagerapp.model.SiteDetailClass
import com.robosoft.passwordmanagerapp.model.UserClass

class UserDataRepository(val myUserDAO: MyUserDAO, val myUserSiteDao: MyUserSiteDao) {


    suspend fun insertUserData(userClass: UserClass): Long{
        return (myUserDAO.insertUser(userClass))
    }

    fun getUserByPhone(userMobile: String): List<UserClass> {
        return (myUserDAO.getUser(userMobile))
    }
    suspend fun insertSiteData(siteDetailClass: SiteDetailClass): Long {
        return myUserSiteDao.insertUserSiteData(siteDetailClass)
    }

    fun getSiteByMobileNumber(userMobile: String): List<SiteDetailClass> {
        return myUserSiteDao.getUserSite(userMobile)
    }
    fun getSiteByID(siteID: Int): List<SiteDetailClass> {
        return myUserSiteDao.getSiteByID(siteID)
    }
}