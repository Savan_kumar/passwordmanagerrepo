/*
 * *****************************************************************************
 * AddSiteActivity.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 2/10/20 1:33 AM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.view

import android.content.Context
import android.content.res.ColorStateList
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import com.robosoft.passwordmanagerapp.R
import com.robosoft.passwordmanagerapp.databinding.ActivityAddSiteBinding
import com.robosoft.passwordmanagerapp.model.database.*
import com.robosoft.passwordmanagerapp.viewmodel.AddSiteViewModelClass
import kotlinx.android.synthetic.main.activity_add_site.*
import kotlinx.android.synthetic.main.activity_pass_home.*

class AddSiteActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
        lateinit var binding: ActivityAddSiteBinding
        lateinit var addSiteViewModel: AddSiteViewModelClass
        lateinit var context: Context
        override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
            binding = DataBindingUtil.setContentView(this, R.layout.activity_add_site)
            setUpToolBar()
            context = this
            val userDao: MyUserDAO = PassManagerDatabase.getInstance(context).myUserDAO
            val siteDao: MyUserSiteDao = PassManagerDatabase.getInstance(context).myUserSiteDao
            val factory = PassManagerFactory(UserDataRepository(userDao, siteDao))
            addSiteViewModel = ViewModelProvider(this, factory).get(AddSiteViewModelClass::class.java)
            binding.lifecycleOwner = this
            binding.addSiteViewModel = addSiteViewModel
            setUpSpinner()
            var sharedPreferences = getSharedPreferences("MyPassPrefs", 0)
            var userMobileNumber = sharedPreferences.getString("userMobileNumber", "")
            //var intentValue = intent
            //var userMobileNumber =  intentValue.getStringExtra("userMobileNumber")
            val icon = resources.getDrawable(R.drawable.base_line_error)
            icon?.setBounds(
                0, 0,
                icon.intrinsicWidth,
                icon.intrinsicHeight
            )
            addSiteViewModel.onInitialExecution.observe(this, Observer {
                if(it) {
                    addSiteViewModel.userMobileNumber.value = userMobileNumber
                }
            })
            addSiteViewModel.isUrlEmpty.observe(this, Observer {
                if(it) {
                    binding.etURLValue.setError("URL cannot be empty", icon)
                    binding.etURLValue.requestFocus()
                }
            })
            addSiteViewModel.isSiteNameEmpty.observe(this, Observer {
                if(it) {
                    binding.etSiteNameValue.setError("Site Name cannot Be empty", icon)
                    binding.etSiteNameValue.requestFocus()
                }
            })
            addSiteViewModel.isSiteTypeEmpty.observe(this, Observer {
                if(it) {
                    binding.etSectorFolderValue.setError("Choose site type", icon)
                    binding.etSectorFolderValue.requestFocus()
                }
            })
            addSiteViewModel.isSiteUserNameEmpty.observe(this, Observer {
                if(it) {
                    binding.etUserNameValue.setError("Enter User Name", icon)
                    binding.etUserNameValue.requestFocus()
                }
            })
            addSiteViewModel.isSiteUserPasswordEmpty.observe(this, Observer {
                if(it) {
                    Toast.makeText(this, "Enter Password", Toast.LENGTH_LONG).show()
                    binding.etUserPasswordValue.requestFocus()
                }
            })
            addSiteViewModel.siteUserPasswordValue.observe(this, Observer {
                when {
                    it == null -> addSiteViewModel.progressRate.value = 0
                    it.length <= 6 -> addSiteViewModel.progressRate.value = 25
                    it.length <= 12 -> addSiteViewModel.progressRate.value = 50
                    it.length > 12 -> addSiteViewModel.progressRate.value = 100
                }
            })
            addSiteViewModel.progressRate.observe(this, Observer {
                if(it == 0 || it == 25) binding.addProgressBar.progressTintList = ColorStateList.valueOf(resources.getColor(R.color.weakPassword))
                else if(it == 50)  binding.addProgressBar.progressTintList = ColorStateList.valueOf(resources.getColor(R.color.intermediatePassword))
                else if(it ==100) binding.addProgressBar.progressTintList = ColorStateList.valueOf(resources.getColor(R.color.strongPassword))
            })
            addSiteViewModel.afterAdd.observe(this, Observer {
                if(it) {
                    finish()
                }
            })
            binding.passToggleAddSite.setOnCheckedChangeListener { buttonView, isChecked ->
                if(isChecked) {
                    binding.etUserPasswordValue.transformationMethod = HideReturnsTransformationMethod.getInstance()
                } else {
                    binding.etUserPasswordValue.transformationMethod = PasswordTransformationMethod.getInstance()
                }
            }

    }

    private fun setUpSpinner(){
        val siteTypes = resources.getStringArray(R.array.site_types)
        val spinnerAdapter = ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, siteTypes)
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerViewAddSite.adapter = spinnerAdapter
        spinnerViewAddSite.onItemSelectedListener = this

    }

    private fun setUpToolBar() {
        supportActionBar?.title = "Add Site"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
      //
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        var textFromSpinner: String = parent?.getItemAtPosition(position).toString()
        binding.etSectorFolderValue.text = textFromSpinner
    }
}