/*
 * *****************************************************************************
 * PassManagerFactory.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 1/10/20 4:50 PM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.model.database


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.robosoft.passwordmanagerapp.viewmodel.*
import java.lang.IllegalArgumentException

class PassManagerFactory(val dataRepository: UserDataRepository): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(SignUpViewModelClass::class.java)){
            return SignUpViewModelClass(dataRepository) as T

        } else if(modelClass.isAssignableFrom(SignInViewModelClass::class.java)){
            return SignInViewModelClass(dataRepository) as T

        }
        else if(modelClass.isAssignableFrom(PassHomeViewModel::class.java)){
            return PassHomeViewModel(dataRepository) as T

        }else if(modelClass.isAssignableFrom(AddSiteViewModelClass::class.java)){
            return AddSiteViewModelClass(dataRepository) as T

        }else if(modelClass.isAssignableFrom(EditSiteViewModelClass::class.java)){
            return EditSiteViewModelClass(dataRepository) as T

        } else throw IllegalArgumentException("Unknown View Model class")
    }

}