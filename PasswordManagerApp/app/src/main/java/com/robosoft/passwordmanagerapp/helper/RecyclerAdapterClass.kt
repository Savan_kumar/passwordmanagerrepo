/*
 * *****************************************************************************
 * RecyclerAdapterClass.kt
 * PasswordManagerApp
 *
 * Created by savankumar on 2/10/20 1:49 PM.
 * Copyright (c) 2020 Robosoft Technologies. All rights reserved.
 *
 *  ****************************************************************************
 */

package com.robosoft.passwordmanagerapp.helper

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.robosoft.passwordmanagerapp.R
import com.robosoft.passwordmanagerapp.databinding.RecyclerviewItemLayoutBinding
import com.robosoft.passwordmanagerapp.model.SiteDetailClass
import com.robosoft.passwordmanagerapp.view.EditSiteActivity


class RecyclerAdapterClass: RecyclerView.Adapter<DataViewHolder>() {
    var siteList = ArrayList<SiteDetailClass>()
    lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        context = parent.context
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: RecyclerviewItemLayoutBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.recyclerview_item_layout, parent, false)

        return DataViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return siteList.size
    }

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        var siteData = siteList[position]
        holder.bindData(siteData)

        holder.binding.recyclerItemView.setOnClickListener {
            //start intent
            val intent = Intent(context, EditSiteActivity::class.java)
            intent.putExtra("siteID", siteData.siteID)
            intent.putExtra("userMobileNumber", siteData.userMobileNumber)
            context.startActivity(intent)
        }

        holder.binding.tvRecyclerCopyPassword.setOnClickListener {
            var clipboard: ClipboardManager = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            var clip = ClipData.newPlainText(context.getString(R.string.copied_password), decrypt(siteData.siteUserPassword, siteData.siteIVValue, siteData.siteCipherKey))
            clipboard?.setPrimaryClip(clip)
            Toast.makeText(context, "Copied to Clipboard", Toast.LENGTH_SHORT).show()
        }
        holder.binding.tvRecyclerUrl.setOnClickListener {
            var urlString = holder.binding.tvRecyclerUrl.text.toString()
            if (!urlString.startsWith("http://") && !urlString.startsWith("https://")){
                urlString = "https://$urlString";
            }
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(urlString))
            context.startActivity(browserIntent)
        }
    }

    fun filterListData(filterList: ArrayList<SiteDetailClass>) {
        siteList = filterList
        notifyDataSetChanged()
    }

}




class DataViewHolder(val binding: RecyclerviewItemLayoutBinding): RecyclerView.ViewHolder(binding.root) {


    fun bindData(siteDetail: SiteDetailClass) {
        binding.tvRecyclerSiteName.text = siteDetail.siteName
        binding.tvRecyclerUrl.text = siteDetail.siteURL
        when (siteDetail.siteName.toLowerCase()){
            "facebook" ->  binding.ivRecyclerAppIcon.setImageResource(R.drawable.facebook)
            "twitter" -> binding.ivRecyclerAppIcon.setImageResource(R.drawable.twitter)
            "youtube" -> binding.ivRecyclerAppIcon.setImageResource(R.drawable.youtube)
            "instagram" -> binding.ivRecyclerAppIcon.setImageResource(R.drawable.instagram)
            else
            -> binding.ivRecyclerAppIcon.setImageResource(R.drawable.ic_baseline_logo)
        }
    }
}